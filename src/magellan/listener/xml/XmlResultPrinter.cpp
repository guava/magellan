#include <magellan/listener/xml/XmlResultPrinter.h>

MAGELLAN_NS_BEGIN

XmlResultPrinter::XmlResultPrinter()
{

}

XmlResultPrinter::~XmlResultPrinter()
{

}

void XmlResultPrinter::startTestRun(const Test&, TestResult&)
{

}

void XmlResultPrinter::endTestRun(const Test&, TestResult&)
{

}

void XmlResultPrinter::startSuite(const Test&)
{

}

void XmlResultPrinter::endSuite(const Test&)
{

}

void XmlResultPrinter::startTest(const Test&)
{

}

void XmlResultPrinter::endTest(const Test&)
{

}

void XmlResultPrinter::addFailure(const TestFailure&)
{

}

MAGELLAN_NS_END
